# Droplet
### About
Droplet is a small website about renewable energy and how it can help us in day-to-day life. This website is part of an independant school project about the `UN Sustainable Goals` and how we can impact them by `2030`.
### How to run
Soon there will be a website up and running with a not-yet decided domain name. For now, you can type in a **Linux** terminal: `python3 droplet.py` or `python droplet.py` if you are on **Windows**. This will create a locally-hosted website that you can view by traveling to this url: `127.0.0.1:5000`.